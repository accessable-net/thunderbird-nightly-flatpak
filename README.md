## How to install

<a href='https://gitlab.com/accessable-net/thunderbird-nightly-flatpak/-/raw/main/thunderbird-nightly.flatpakref?inline=false'><img width='240' alt='Download on Flathub' src='assets/download-badge.svg'/></a>

Or at the command line,

````bash
flatpak install --user https://gitlab.com/accessable-net/thunderbird-nightly-flatpak/-/raw/main/thunderbird-nightly.flatpakref
````
